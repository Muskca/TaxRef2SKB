/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package taxref2rkb;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author murloc
 */
public class TaxRef2RKB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
//        SparqlProxy spOut = SparqlProxy.getSparqlProxy("http://amarger.murloc.fr:8080/TaxRef2RKB_out_TEST/");
//        
//        System.out.println("Clear previous Sparql Endpoint...");
//        spOut.clearSp();
//        System.out.println("Sparql Endpoint cleared!");
        
        TaxRefExtractor tre = new TaxRefExtractor();
        tre.loadRef("in/TAXREFv70_utf8.txt");
        
        StringBuilder out = new StringBuilder();
        System.out.println("Export ontological module prefixes ... ");
        try {
            String modulePrefixes = FileUtils.readFileToString(new File("in/agronomicTaxon_prefix.ttl"), "utf-8");
            out.append(modulePrefixes);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Prefixes exported");
        
        System.out.println("Export ontological module ... ");
        try {
            String moduleTriples = FileUtils.readFileToString(new File("in/agronomicTaxon_2.ttl"), "utf-8");
            out.append(moduleTriples);
        } catch (IOException ex) {
            System.err.println("Error during prefixes export : ");
            System.err.println(ex);
        }
        System.out.println("Module exported");
        System.out.println("Filtering sub part ...");
        // Triticum  :198676
        // Plantae : 187079
        out.append(tre.getSubpart("198676"));
        out.append(tre.getUpperPart("198676")); //
        System.out.println("Sub part filtered");
        try {
            FileUtils.write(new File("out_TaxRef.ttl"), out);
        } catch (IOException ex) {
            System.err.println("FILE WRITTER ERROR");
        }
    }
    
}
